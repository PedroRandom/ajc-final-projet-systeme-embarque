#ifndef MODVILLE_H
#define MODVILLE_H
#include "incomexception.h"
#include "infclass.h"
#include <QApplication>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslSocket>
#include <map>

void contactWeb(InfVill*);
QJsonObject obtJson(QUrl,QString,InfVill*);

#endif // MODVILLE_H
