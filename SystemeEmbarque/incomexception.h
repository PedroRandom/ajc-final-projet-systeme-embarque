#ifndef INCOMEXCEPTION_H
#define INCOMEXCEPTION_H
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <QString>
#include <QResource>
#include <QSettings>
#include <QTranslator>
#define actuHeure 1000
#define actuInfo 60000
#define comments 0
#define fDones "lecture.txt"
#define idIpPy "pi@78.194.1.105" // Id de usuaire @ Direction Ip du Raspberry Py
#define portPy "22000" //Port qu'on va a utiliser
#define softEmbarque "'bme280'" // Executable qu'on va a lancer dans la Raspberry Py
#define imgForm ".png" // Format d'image choisi
#define imgUnknown "unknown"
#define paramFile "parametres.ini"
#define reqUrlBas "http://api.openweathermap.org/data/2.5/"
#define appId "&appid=e3fafe839d49f34b990ea35a4c4cfce1&"
#define matin " 12:"
#define soir  " 21:"
#define sunSet 20
#define sunRise 8
#define colDefaut "black"
#define famDefaut "Arial"
#define vilDefaut "Toulouse"
#define maxPrev 10 // 5 Prev Matin 5 Prev Soirs
#define fSize 12
#define transPath "C:/Qt/5.15.2/mingw81_64/translations/qtbase_en.qm"

using namespace std;

enum class ErrCode{NO_ERR,ERR_COM,ERR_NF,ERR_TMP,ERR_FIC,ERR_SSL,ERR_POL,ERR_COL,ERR_VIL,ERR_CODE};

class InComException : public exception
{
    public:
        InComException(ErrCode cod=ErrCode::NO_ERR,string text="");
        virtual ~InComException();
        virtual const char* what() const throw() override;
        ErrCode getCode() const {return this->code;}

    private:
        ErrCode code; /**< Code de l'erreur produit */
        string text; /**< Text que complement l'erreur produit */
        mutable string expErr;/**< Explication de l'erreur produit */
};

#endif // INCOMEXCEPTION_H
