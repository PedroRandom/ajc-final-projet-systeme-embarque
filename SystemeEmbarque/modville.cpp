#include "modville.h"
QJsonObject obtJson(QUrl url,QString jsonHeader,InfVill *iVill)
{
    /*** Fonction qui sert a obtenir l'objet Json a partir d'une URL ***/
    QNetworkAccessManager nam; // Variable utilisé pour faire la requet web à l'URL
    QNetworkReply *reply;  // Variable qui garde la reponse de la requete web
    QByteArray responseData; // Variable qui garde la data de la reponse dans le format QByteArray
    QJsonDocument jsonResponse; // Variable utilisé pour garde la conversion de la data du format QByteArray avec le format JsonDocument (Ce format permet manipuler part de l'information)
    QRegExp code("2[0-9]{2}"); // Utilisé pour les codes d'HTTP - 2XX = Succes, si non = erreur
    QString compErr=""; // Utilise pour completer l'info de l'erreur
    // Test du platform SSL
    if(!QSslSocket::supportsSsl())
    {
        iVill->setErr(InComException(ErrCode::ERR_SSL));
        throw iVill->getErr();
    }
    // Set l'URL du request
    QNetworkRequest request(url);
    // Set du header
    request.setHeader(QNetworkRequest::ContentTypeHeader, jsonHeader);
    // Demande de l'information
    reply = nam.get(request);

    // On attend que le telechargement de l'information soit complet
    while(!reply->isFinished())
    {
        qApp->processEvents();
    }
    reply->deleteLater();
    // Lecture du data
    responseData = reply->readAll();
    // Conversion du ByteArryay en Json Document
    jsonResponse = QJsonDocument::fromJson(responseData);
    // Test des posibles erreur
    if(jsonResponse.isEmpty())
    {//Erreur conexion de internet
        compErr ="Verifiez la conexion a internet ou si le site web est accesible.";
    }
    else if (jsonResponse["message"].toString().indexOf("city not found")!=-1)
    {//Erreur de ville non trouvé
        compErr =QString("La ville '%1' n'a été pas trouvé dans le site web").arg(iVill->getVille());
    }
    else if (jsonResponse["message"].toString().indexOf("API")!=-1)
    {//Erreur de mauvais code API
        compErr = "Key API invalide, revisez cette code dans incomexception.h";
    }
    else if (!code.exactMatch(jsonResponse["cod"].toVariant().toString())) // De fois le code est retourne comme string et de fois comme double, c'est pour ça qu'il faut premier le convertir a variant pour pouvoir le convertir a string (quand le JsonValue est de type double, la conversion directe a string echoue )
    {//Le code retourne par le JSON n'est pas 2XX, pour tant il y a un erreur HTTP
        compErr = QString("Le protocole HTTP a retourne le code d'erreur suivant %1, revisez le fichier modville.cpp et incomexception.h").arg(jsonResponse["cod"].toVariant().toString());
    }
    if (!compErr.isEmpty())
    { // Si un erreur a été trouve, il est garde dans iVill et il est lance (throw)
        iVill->setErr(InComException(ErrCode::ERR_COM,QString("la API Web. \n %1 ").arg(compErr).toStdString()));
        throw iVill->getErr();
    }
    // Retour d'un objet Json pour pouvoir manipuler le contenu
    return jsonResponse.object();

}

void contactWeb(InfVill *iVill)
{
    /*** Fonction qui sert a garder les informations des previsions obtenus de l'API Web sur InfVill ***/
    // Creation d'un map pour des keys du JSON pour faciliter la manutention du code
    map<string,QString> keys= {
        {"jsonHead","application/json"},{"name","name"},
        {"main","main"},{"temp","temp"},
        {"humid","humidity"}, {"pres","pressure"},
        {"weather","weather"},{"img","icon"},
        {"list","list"},{"date","dt_txt"}};
    QJsonObject objJSON; //Objet avec l'information du JSON
    QVariantMap json_map, temp; // Variable utilise pour gerer les differents maps (temp) dans le map principale (json_map)
    QJsonArray jsonArray;

    //*********************************************//
    //********** PREVISION METEO ACTUELLE *********//
    //*********************************************//
    //Obtention du objet JSON a partir de l'URL (header mis comme JSON)
    objJSON = obtJson(QUrl(QString("%1%2%3%4")
                           .arg(reqUrlBas)// Bas de la url à laquelle on demande de la information
                           .arg("weather?lang=fr&units=metric&q=") // Type d'info (prevision actuelle), language, unites et ville
                           .arg(iVill->getVille()) // Nom de la ville a rechercher
                           .arg(appId)),// Cle d'identification pour l'API web
                      keys["jsonHead"],// Definition du header comme JSON
                      iVill);

    json_map = objJSON.toVariantMap(); // Conversion a Variant map pour pouvoir acceder a l'information des sub-elements
    iVill->setDate(json_map[keys["date"]].toString());
    temp = json_map[keys["main"]].toMap(); // Conversion du contenu du main a map pour pouvoir acceder a ces membres
    iVill->setTemp(temp[keys["temp"]].toDouble());//Récupération de l'info température du JSON et enregistrement dans l'structure
    iVill->setHumid(temp[keys["humid"]].toDouble());//Récupération de l'info Humidité du JSON et enregistrement dans l'structure
    iVill->setPres(temp[keys["pres"]].toDouble());//Récupération de l'info Pression du JSON et enregistrement dans l'structure

    temp = json_map[keys["weather"]].toList()[0].toMap();// Conversion du contenu du "weather" a list, acces au premier membre (il y a seulement la information d'ajourd'hui) et conversion a map pour pouvoir acceder a ces membres
    iVill->setIcon(temp[keys["img"]].toString());//Récupération du numéro de l'icon via l'API

    //*********************************************//
    //************ PREVISION SUR 5 JOURS **********//
    //*********************************************//
    //Obtention du objet JSON a partir de l'URL (header mis comme JSON)
    objJSON =obtJson(QUrl(QString("%1%2%3%4")
                          .arg(reqUrlBas) // Bas de la url à laquelle on demande de la information
                          .arg("forecast?lang=fr&units=metric&q=") // Type d'info (prevision pour 5 jours), language, unites et ville
                          .arg(iVill->getVille()) // Nom de la ville a rechercher
                          .arg(appId)), // Cle d'identification pour l'API web
                          keys["jsonHead"], // Definition du header comme JSON
                          iVill);

    //Positionne sur l'élément "list" et conversion a array pour pouvoir acceder aux sub-elements
    jsonArray = objJSON[keys["list"]].toArray();

    iVill->clearPrev(); // On nettoye la liste de previsions dans iVill avant de la remplir a nouveaux
    // Il y a 3 scenarios:
    //  - Heure 1er prevision < matin --> Fonctionement normal du prevision 5 jours (Ajourd'hui a J+4)
    //  - Heure 1er prevision > matin et <= soir --> On prend la prevision actuelle com premier prevision (et on evite prendre la information du matin du J+1)
    //  - Heure 1er prevision > soir --> Fonctionement normal mais le premier journe sera le jour suivant (J+1 a J+5)
    foreach(const QJsonValue &value, jsonArray)
    {
        InfJournee inf; // Element temporelle qu'on rajoutera à la list de previsions futures

        json_map = value.toObject().toVariantMap(); // Transformation du JSon value a objet et a variant map pour pouvoir acceder a l'information dans le sub-elements

        inf.setDate(json_map[keys["date"]].toString());//Récupération de l'info Date du JSON

        if (iVill->getLstPrev().size()==0)
        { // Test de l'heure de la 1er prevision
            double heure1erPrev = inf.getDate().split(" ")[1].split(":")[0].toDouble();
            if (heure1erPrev>QString().fromStdString(matin).split(" ")[1].split(":")[0].toDouble()
            &&  heure1erPrev<=QString().fromStdString(soir).split(" ")[1].split(":")[0].toDouble())
            { // Si l'heure est entre matin et soir, on l'additione
                inf.setTemp(iVill->getTemp());
                inf.setPres(iVill->getPres());
                inf.setHumid(iVill->getHumid());
                inf.setIcon(iVill->getIcon());
                *iVill+inf;
            }
        }
        if (inf.getDate().indexOf(matin)!=-1 || inf.getDate().indexOf(soir)!=-1) //On utilisera que l'information d'une heure du matin et une heure du soir
        {
            temp = json_map[keys["main"]].toMap();
            inf.setTemp(temp[keys["temp"]].toDouble());//Récupération de l'info température du JSON
            inf.setPres(temp[keys["pres"]].toDouble());//Récupération de l'info pression du JSON
            inf.setHumid(temp[keys["humid"]].toDouble());//Récupération de l'info humidité du JSON

            temp = json_map[keys["weather"]].toList()[0].toMap(); // Conversion du contenu du "weather" a list, acces au premier membre (prevision qui nous interesse) et conversion a map pour pouvoir acceder a ces membres
            inf.setIcon(temp[keys["img"]].toString());//Récupération du numéro de l'icon du JSON

            *iVill+inf; // Rajout de la prevision inf dans le vecteur de previsions futures prevJournees au siend du InfVill
        }
        if(iVill->getLstPrev().size()==10)
        {
            break;
        }
    }
}
