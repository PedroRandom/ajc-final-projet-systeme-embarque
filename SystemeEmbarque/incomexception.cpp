#include "incomexception.h"


InComException::InComException(ErrCode cod, string t)
    : code(cod),text(t)
{

}

InComException::~InComException()
{
    if(comments)
    {
        cout<<"Destruction du exception "<<endl;
    }
}

const char* InComException::what() const throw()
{
    ostringstream oss;
    string message;
    switch(this->code)
    {
        case ErrCode::NO_ERR:
            oss<<"Il n'y a pas d'erreur (PLACE HOLDER)."<<endl;
            break;
        case ErrCode::ERR_COM:
            oss<<"La comunication n'a pas pu etre entabli avec "<<this->text<<endl;
            break;
        case ErrCode::ERR_NF:
            oss<<"La village cherche "<<this->text<<" n'a ete pas trouve dans les villes disponibles, "<<vilDefaut<<" sera mis par defaut"<<endl;
            break;
        case ErrCode::ERR_TMP:
            oss<<"La temperature lis est dehors du range admis (-40 à 50 °C)"<<endl;
            break;
        case ErrCode::ERR_FIC:
            oss<<"Le fichier '"<<this->text<<"' n'a pas pu etre lis. Verifiez l'statut du fichier"<<endl;
            break;
        case ErrCode::ERR_SSL:
            oss<<"Un erreur est apparu avec le SSL, verifiez que vous avez installe un logiciel de protocole de communication SSL/TLS comment OpenSSL dans votre ordinateur"<<endl;
            break;
        case ErrCode::ERR_POL :
            oss<<"La police defini "<<this->text<<" n'est pas trouvé dans les police disponibles, "<<famDefaut<<" sera mis par defaut"<<endl;
            break;
        case ErrCode::ERR_COL :
            oss<<"Le coleur defini "<<this->text<<"n'est pas trouvé dans les coleurs disponibles, "<<colDefaut<<" sera mis par defaut"<<endl;
            break;
        case ErrCode::ERR_CODE :
        default:
            oss<<"Erreur non gere :"<<this->text<<endl;
            break;
    }
    this->expErr=oss.str(); // Il faut le garder dans l'objet parce que si non, la explication serait detruit avant de pouvoir etre lis au main
    return this->expErr.c_str();
}
