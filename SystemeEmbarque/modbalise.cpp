#include "modbalise.h"
void getErreurs(InfBal *iBal,QProcess* process)
{
    /*** Fonction utilisé pour capter et gerer tous les erreurs qui peuvent apparetre apres d'avoir lance un process, la information de l'erreur est garde sur iBal***/

    QString erreur=""; // Utilise pour stocker le stdErr des commandes
    QString compErr=""; // Utilise pour complementer l'erreur
    if (comments)
    {
        qDebug(process->readAllStandardOutput());
    }
    erreur=process->readAllStandardError(); // Il y a la option aussi d'utiliser la signal d'erreur mais dans cette cas-là on utilise cette version simplifie

    // Gestion des erreurs
    if (!erreur.isEmpty())
    {
        if (erreur.indexOf("Connection timed out")!=-1)
        {
            compErr="Verifiez que la ip et port definis dans incomexception.h sont corrects.";
        }
        else if (erreur.indexOf("Unknown error")!=-1) {
            compErr="Possiblement la source du erreur est lie à la conexion a internet.";
        }
        else if (erreur.indexOf("command not found")!=-1)
        {
            compErr="Verifiez que le commande definis dans incomexception.h correspond au nom du executable dans Raspberry Pi";
        }
        else if (erreur.indexOf("No such file or directory")!=-1)
        {
            compErr="Verifiez que le nom du fichier genere dans la Raspberry PI est celle definis dans inputexception.h";
        }
        iBal->setErr(InComException(ErrCode::ERR_COM,QString("la raspberry PI.\n %1 \n Message du commande ssh :\n %2").arg(compErr).arg(erreur).toStdString()));
        throw iBal->getErr();
    }
}
void launchProcess(QString commande,QStringList arguments,InfBal *iBal)
{
    /*** Function utilisé pour lancer un process avec une commande et arguments definis. Aussi gere les possibles erreurs apparus et les garde sur iBal***/
    QProcess *newProcess = new QProcess(nullptr);
    // Init du soft dans raspberry Pi
    newProcess->start(commande, arguments);
    // On attend que l'execution du programme embarque a finis
    newProcess->waitForFinished();

    // Check des erreurs
    getErreurs(iBal,newProcess);
}
void lectureFichier(InfBal *iBal)
{
    QString key=""; // Utilise pour la lecture des keys dans le fichier
    bool openOk=false;

    QFile file(fDones);
    openOk = file.open(QIODevice::ReadOnly|QIODevice::Text);
    QString contenuFichier;

    if (openOk)
    {
        contenuFichier = file.readAll();
    }
    else {
        file.close(); // On ferme le fichier avant de faire la gestion d'erreur
        iBal->setErr(InComException(ErrCode::ERR_FIC,QDir::currentPath().toStdString()
                     +"/"+fDones)); // Dans le future mettre le path complete
        throw iBal->getErr();
    }
    file.close();
    for(QString line:contenuFichier.split("\n"))
    {
        if (line!="")
        {
            // La key c'est l'information qui a a gauche du :
            key = line.split(":")[0];
        }
        // On utilise indexOf pour eviter des problems avec les spaces en blanch
        if(key.indexOf("Humidity")!=-1)
        {
            iBal->setHumid(line.split(":")[1].toDouble());
        }
        else if (key.indexOf("Pressure")!=-1) {
            iBal->setPres(line.split(":")[1].toDouble());
        }
        else if (key.indexOf("Temperature")!=-1) {
            iBal->setTemp(line.split(":")[1].toDouble());
        }
    }
}
void contactBalise(InfBal *iBal)
{
    /*** Fonction qui contact la balise pour faire une lecture du sensor,
    apres envoye un requete pour telecharger le fichier lecture.txt
    et finalement lis le fichier et garde l'information de iBal
    S'il y a des erreurs pendant le process, ils sont detectes et gardes sur iBal***/

    // 1er Comande a executer ssh id@ip -p port 'executable' // Variables definis dans incomexception.h :
    QString initPy = "ssh" ;// Dans program on define l'executable, dans ce cas le ssh
    QStringList arguments; // Dans cette list tous les arguments a passer au executable
    arguments.append(idIpPy); // Premier l'usuaire @ l'adress ip
    arguments.append(QString("-p %1").arg(portPy)); // Definition du port
    arguments.append(softEmbarque); // Ajout de la commande a lancer dans la raspberry pi

    // 2eme Commande a executer : scp -P port id@ip:fichier .
    QString reqDownload = "scp";  // Executable a utiliser par la commande
    QStringList arguments2; // list des arguments
    arguments2<<QString("-P %1").arg(portPy); // Definition du port
    arguments2.append(QString("%1:%2").arg(idIpPy,fDones)); // Definition du destinataire (usuaire@ip garde dans idIpPty) et du fichier a telecharger (fDones)
    arguments2.append("."); // Le fichier se garde sur le directoire d'execution

    // Init du soft dans raspberry Pi
    launchProcess(initPy,arguments,iBal);

    // Telechargement du fichier
    launchProcess(reqDownload,arguments2,iBal);

    // Lecture du fichier
    lectureFichier(iBal);
}
