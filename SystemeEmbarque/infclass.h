#ifndef INFCLASS_H
#define INFCLASS_H
#include "incomexception.h"
#include <iostream>

class Pol // Contient les infos de la police (Famille et Couleur)
{
    public:
        Pol(QString="Arial", QString="Black");
        virtual ~Pol();
        QString getFam(){return this->fam;}
        void setFam(const QString &value);

        QString getCol(){return this->col;}
        void setCol(const QString &value);

        virtual string infos();
private:
        QString fam;
        QString col;

};

class InfBal // Contient les infos de la Balise
{
    public:
        InfBal(double=0, double=0, double=0);
        virtual ~InfBal();
        double getTemp() {return this->temp;}
        void setTemp(double value);

        double getHumid(){return this->humid;}
        void setHumid(double value);

        double getPres(){return this->pres;}
        void setPres(double value);

        InComException getErr(){return this->err;}
        void setErr(const InComException &value);

        virtual string infos();

private:
        double temp; /* Garde la temperature en Celsius*/
        double humid; /*Garde la humidite*/
        double pres; /*Garde la pression*/
        InComException err; /* Garde si un erreur est apparu*/
};
class InfJournee
{
    public:
        InfJournee(QString="",double=0,double=0, double=0,QString="",InComException=InComException());
        virtual ~InfJournee();

        QString getDate() {return this->date;}
        void setDate(const QString &value);

        double getTemp() {return this->temp;}
        void setTemp(double value);

        double getHumid(){return this->humid;}
        void setHumid(double value);

        double getPres(){return this->pres;}
        void setPres(double value);

        QString getIcon() {return this->icon;}
        void setIcon(const QString &value);

        InComException getErr(){return this->err;}
        void setErr(const InComException &value);

        virtual string infos(); // Affiche les infos

        QString getIconFile(); // Retour le FileName du icon pour pouvoir le montrer dans l'ecran, aussi gere les cas non connus

private:
        QString date; /*Garde la date et heure de la prevision*/
        double temp; /* Garde la temperature en Celsius*/
        double humid; /*Garde la humidite*/
        double pres; /*Garde la pression*/
        QString icon; /*Garde le nom de l'image du Pictogram*/
        InComException err; /* Garde si un erreur est apparu*/
};

class InfVill : public InfJournee // Contient les infos de la ville
{
    public:
        InfVill(QString="Maintenant",double=0, double=0,double=0,QString="",QString="A saisir",InComException=InComException());
        virtual ~InfVill();

        QString getVille(){return this->ville;}
        void setVille(const QString &value);

        vector<InfJournee> getLstPrev() {return this->lstPrev;}
        void setLstPrev(const vector<InfJournee> &value);

        void addPrev(InfJournee prev){lstPrev.push_back(prev);}
        void clearPrev(){this->lstPrev.clear();}

        void operator+ (InfJournee prev){lstPrev.push_back(prev);}

        virtual string infos() override; // Affiche les infos
private:
        QString ville; /*Garde le nom de la ville consulté*/
        vector<InfJournee> lstPrev;
};
class InfOptions // Contient les infos selectione dans l'interface graphique
{
    public:
        InfOptions(bool=true,bool=true,bool=true,bool=true,QString="A choisir",
                   QString="Arial",QString="Black", // Police Jour
                   QString="Arial",QString="Black", // Police Nuit
                   QString=QResource(":/english.qm").fileName(),QString=transPath,bool=false);
        virtual ~InfOptions();

        bool getFrmtHr12(){return this->frmtHr12;}
        void setFrmtHr12(bool value);

        bool getFrmtTmpCel(){return this->frmtTmpCel;}
        void setFrmtTmpCel(bool value);

        bool getLangueFr(){ return this->langueFr;}
        void setLangueFr(bool value);

        bool getModeJour() {return this->modeJour;}
        void setModeJour(bool value);

        Pol *getPolJour() {return this->polJour;}
        void setPolJour(Pol *value);

        Pol *getPolNuit() {return this->polNuit;}
        void setPolNuit(Pol *value);

        QString getVilleChoix() {return this->villeChoix;}
        void setVilleChoix(const QString &value);

        void loadParams(); // Charge les options gardes dans le fichier de params
        void saveParams(); // Garde les options choisis/utilisés dans le fichier de params

        virtual string infos();

        QTranslator t;
        QTranslator guiTranslator;
        bool instaled;

private:
        static vector<QString> paramNames; // Garde les strings utilises pour garder l'info de la structure dans le fichier parametre.inic
        bool frmtHr12; /*True si le format est de 12 Hr // False Format 24 Hr*/
        bool frmtTmpCel; /* True si la temperature est en Celsius // False T en Farenheit*/
        bool langueFr; /* True si le langage d'affichage est français // False Langue en Anglais*/
        bool modeJour; /* True si on utilise la Police Jour // False Police Nuit*/
        QString villeChoix;
        Pol* polJour; /* Contient la Police a utiliser dans le mode jour*/
        Pol* polNuit; /* Contient la Police a utiliser dans le mode nuit*/

};

class InfClass
{
    public:
        InfClass(bool=true,bool=true,bool=true,bool=true,QString="A choisir" ,QString="Arial", QString="Black",QString="Arial",QString="Black",  // Infos du Interface Graphique
                 double=0, double=0, double=0, // Infos du Balise
                 QString="maintenant",double=0, double=0,double=0,QString="",QString="A saisir",InComException=InComException()); // Infos du Village
        virtual ~InfClass();

        InfBal *getInfosBalise() {return this->infosBalise;}
        void setInfosBalise(InfBal *value);

        InfVill *getInfosVille() {return this->infosVille;}
        void setInfosVille(InfVill *value);

        InfOptions *getInfosOptions(){return this->infosOptions;}
        void setInfosOptions(InfOptions *value);

        virtual string infos();

        static double tempFromCtoF(double=0,bool=false);
        static QString genTstring(double=0,bool=true);

private:
        InfBal* infosBalise; /* Contient les informations provenant de la balsie, pointeur parce qu'il sera manipule dans la fonction specialise*/
        InfVill* infosVille;/* Contient les informations provenant du api Web, pointeur parce qu'il sera manipule dans la fonction specialise*/
        InfOptions* infosOptions; /*Contient les informations configures avec l'interface graphique*/
};

#endif // INFCLASS_H
