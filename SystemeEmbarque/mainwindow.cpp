#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTranslator>


#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("MeteoStation");
    this->setUpTimers();
    QTimer::singleShot(100,this,SLOT(actuFont()));// La font doit etre mis a jour une fois la fenetre a ete deja ouvert pour eviter bloquer le setFont, donc un utilise un timer de singleShot
    this->actuWindow();
}

MainWindow::~MainWindow()
{
    delete ui;
    if (comments)
    {
        cout<<"Destruction du fenetre principal"<<endl;
        cout<<"Destruction du timer et objet Heure"<<endl;
    }
    delete heure;
    delete timerHeure;
    if (comments){    cout<<"Destruction du timer Infos"<<endl;}
    delete timerInfos;
    if (comments) {cout<<"Destruction de infos"<<endl;}
    delete info;
}

void MainWindow::setUpTimers()
{
    this->info=new InfClass;
    this->initText();
    this->info->getInfosOptions()->setModeJour(this->modeJ());
    this->info->getInfosVille()->setVille(this->info->getInfosOptions()->getVilleChoix());
    actuTime(); // Init de l'heure (t=0)
    this->setWin = new SettingsWindow(this,this->info->getInfosOptions());
    this->setWin->showMinimized(); //On ouvre et ferme la fenetre setWin pour eviter le blockage du Font --> A chercher mieux solution dans un future
    this->setWin->close();
    this->actuBckg(true); //On mis a jour l'image du background
    actuInfos(); // Init des infos (t=0)
    timerHeure = new QTimer();
    timerHeure->setInterval(actuHeure); // actuHeure defini dans InComException.h
    timerHeure->start();
    connect(timerHeure,SIGNAL(timeout()),this,SLOT(actuTime()));
    timerInfos= new QTimer();
    timerInfos->setInterval(actuInfo); // actuInfo defini dans InComException.h
    timerInfos->start();
    connect(timerInfos,SIGNAL(timeout()),this,SLOT(actuInfos()));
}

void MainWindow::initText()
{   //Initialise les text fixes de l'ecran principale
    vector<QString> texts={"Température : ","Humidité : ","Pression : "};
    vector<QLabel*> balise={ui->LblBaliseTemp,ui->LblBaliseHum,ui->LblBalisePres}; // Labels a metre a jour de la balise
    vector<QLabel*> ville ={ui->LblVilleTemp,ui->LblVilleHum,ui->LblVillePres};
    //Mis du texts dans les labels concernant la Balise et la ville
    this->setTextLabels(balise,texts);
    ui->LblVille->setText("Ville : ");
    this->setTextLabels(ville,texts);

    //Graphique de previsions
    //Background transparent:
    ui->customPlot->setBackground(Qt::GlobalColor::transparent);
    ui->customPlot->setAttribute(Qt::WA_OpaquePaintEvent, false);
    // make top and right axes invisible
    ui->customPlot->xAxis->setVisible(false);
    ui->customPlot->yAxis->setVisible(false);
}

void MainWindow::setTextLabels(vector<QLabel *> labelUI, vector<QString> text)
{   //Mis le text dans les labels , ils doivent avoir le meme longueur
    if (labelUI.size()!=text.size())
    {
        throw InComException(ErrCode::ERR_CODE,"Incorrect utilisation du function setTextLabels, il doit avoir la meme cantite de text que de labels a remplir");
    }
    for(int i=0;i<labelUI.size();i++)
    {
        labelUI[i]->setText(text[i]);
    }
}

void MainWindow::actuBalText()
{   //Définition des labels liés aux valeurs infos Balise
    vector<QLabel*> lbls = {ui->lblTvalBal,ui->lblHmdValBal,ui->lblPvalBal};
    vector<QString> vals = {
          InfClass::genTstring(this->info->getInfosBalise()->getTemp(),this->info->getInfosOptions()->getFrmtTmpCel()),
          QString::number(this->info->getInfosBalise()->getHumid())+" %",
          QString::number(this->info->getInfosBalise()->getPres())+" hPa"};
    this->setTextLabels(lbls,vals);
}

void MainWindow::actuVilleText()
{   //Définition des labels liés aux valeurs infos Ville
    vector<QLabel*> lbls= {ui->lblVilleName,ui->lblTvalVille,ui->lblHmdvalVille,ui->lblPvalVille};
    vector<QString> vals = {
        this->info->getInfosVille()->getVille(),
        InfClass::genTstring(this->info->getInfosVille()->getTemp(),this->info->getInfosOptions()->getFrmtTmpCel()),
        QString::number(this->info->getInfosVille()->getHumid())+" %",
        QString::number(this->info->getInfosVille()->getPres())+" hPa"};
    this->setTextLabels(lbls,vals);
    //Affichage de l'icône concernant la météo actuelle
    ui->lblImage->setPixmap(QPixmap(this->info->getInfosVille()->getIconFile()).scaled(ui->lblImage->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

void MainWindow::actuGraph()
{
    vector<QLabel*> icons = {
        ui->IconJour1,ui->IconNuit1,ui->IconJour2,
        ui->IconNuit2,ui->IconJour3,ui->IconNuit3,
        ui->IconJour4,ui->IconNuit4,ui->IconJour5,ui->IconNuit5};
    vector<QLabel*> dates = {
        ui->Date1,ui->Date2,ui->Date3,ui->Date4,ui->Date5};
    vector<QString> dayFrmt = {"MM/dd","dd/MM"};
    vector <InfJournee> vectGraph = this->info->getInfosVille()->getLstPrev();
    for(int i=ui->customPlot->graphCount();i>=0;i--)
    {
        ui->customPlot->removeGraph(i);
    }
    ui->customPlot->clearItems();
    if(!vectGraph.empty())
    {
        QVector<QCPGraphData> dataJ(maxPrev/2),dataN(maxPrev/2); // initialize with entries 0..vectGraph.size()
        int iJour = 0,iNuit = 0;
        double minT=99,maxT=-99;
        for (InfJournee inf:vectGraph)
        {
            QCPItemText *textLabel = new QCPItemText(ui->customPlot);
            textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
            if ((iJour+iNuit)%2==0)
            {
                dataJ[iJour].key=QVariant().fromValue(inf.getDate()).toDateTime().toTime_t();
                dataJ[iJour].value=InfClass::tempFromCtoF(inf.getTemp(),!this->info->getInfosOptions()->getFrmtTmpCel()); // Avec le FrmtTmpCel on controle si on veux faire ou pas la conversion
                textLabel->position->setCoords(dataJ[iJour].key,dataJ[iJour].value);
                textLabel->setText(QString::number(dataJ[iJour].value));
                icons[iJour+iNuit]->setPixmap(QPixmap(inf.getIconFile()).scaled(icons[iJour+iNuit]->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                dates[iJour]->setText(QVariant(inf.getDate()).toDateTime().toString(dayFrmt[this->info->getInfosOptions()->getLangueFr()]));//mid(5,5));
                iJour++;
            }
            else
            {
                dataN[iNuit].key=dataJ[iJour-1].key;
                dataN[iNuit].value=InfClass::tempFromCtoF(inf.getTemp(),!this->info->getInfosOptions()->getFrmtTmpCel());
                textLabel->position->setCoords(dataN[iNuit].key,dataN[iNuit].value);
                textLabel->setText(QString::number(dataN[iNuit].value));
                icons[iJour+iNuit]->setPixmap(QPixmap(inf.getIconFile()).scaled(ui->IconJour1->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                iNuit++;
            }
            if (InfClass::tempFromCtoF(inf.getTemp(),!this->info->getInfosOptions()->getFrmtTmpCel()) < minT){minT = InfClass::tempFromCtoF(inf.getTemp(),!this->info->getInfosOptions()->getFrmtTmpCel());}
            if (InfClass::tempFromCtoF(inf.getTemp(),!this->info->getInfosOptions()->getFrmtTmpCel()) > maxT){maxT = InfClass::tempFromCtoF(inf.getTemp(),!this->info->getInfosOptions()->getFrmtTmpCel());}
        }

        // create graph and assign data to it:
        ui->customPlot->addGraph();
        ui->customPlot->graph()->data()->set(dataJ);
        //Ajout des points en forme de disque sur la courbe et le coleur:
        ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ssDisc);
        ui->customPlot->graph()->setPen(QPen(QColor("yellow")));
        ui->customPlot->addGraph();
        ui->customPlot->graph()->data()->set(dataN);
        //Ajout des points en forme de disque sur la courbe:
        ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ssDisc);
        ui->customPlot->graph()->setPen(QPen(QColor("darkblue")));
        // set axes ranges, so we see all data:
        ui->customPlot->xAxis->setRange(dataJ[0].key-3600*12, dataJ[iJour-1].key+3600*12);
        ui->customPlot->yAxis->setRange(minT-0.25*qFabs(minT), maxT);
    }
    ui->customPlot->replot();
}
void MainWindow::actuTemp()
{
    vector<QLabel*> lbls= {ui->lblTvalVille,ui->lblTvalBal};
    vector<QString> vals ={InfClass::genTstring(this->info->getInfosVille()->getTemp(),this->info->getInfosOptions()->getFrmtTmpCel()),
                          InfClass::genTstring(this->info->getInfosBalise()->getTemp(),this->info->getInfosOptions()->getFrmtTmpCel())};
    this->setTextLabels(lbls,vals);
    this->actuGraph();
}
void MainWindow::actuWindow()
{
    this->actuBalText();
    this->actuVilleText();
    this->actuGraph();
}

void MainWindow::actuTime()
{
    QString heureActuel,frmt;
    if(this->info->getInfosOptions()->getFrmtHr12())
    {
        frmt="hh:mm:ss AP";
    }
    else
    {
        frmt="HH:mm:ss";
    }
    if (this->info->getInfosOptions()->getLangueFr())
    {
        heureActuel=QLocale("fr_FR").toString(QDateTime().currentDateTime(),QString("d/M/yyyy %1").arg(frmt));
    }
    else
    {
        heureActuel=QLocale("en_EN").toString(QDateTime().currentDateTime(),QString("M/d/yyyy %1").arg(frmt));
    }
    ui->lblHeure->setText(heureActuel);
    QFont tmp(this->font());
    tmp.setPointSize(this->font().pointSize()+2); // Mis d'heure un peux plus grand
    ui->lblHeure->setFont(tmp);
}
bool MainWindow::modeJ()
{// Determine si le mode actuelle en fonction de l'heure
    if (heure->currentDateTime().toString("H").toDouble()>=sunRise
    &&  heure->currentDateTime().toString("H").toDouble()<sunSet)
    {
        return true;
    }
    else{
        return false;
    }

}
void MainWindow::actuBckg(bool init)
{ // Utilisé pour metre a jour le background (init ou verification chaque heure)
    if (!this->modeJ() && // ((h>=sunSet || h<sunRise) && (modeJour||init))
       (this->info->getInfosOptions()->getModeJour() ||  init))
    { // Si on est dans le mode jour ou init et l'heure est maintenant apres le sunSet ou avant sunRise, on mis a jour l'image de fond et le mode a Nuit
        QString img=QResource(":/Background/Images/nuit.jpg").fileName();
        ui->bckImg->setPixmap(QPixmap(img).scaled(this->size()));
        this->setWin->getBckgImg()->setPixmap(QPixmap(img).scaled(this->size()));
        this->info->getInfosOptions()->setModeJour(false);
        this->setWin->setPal(this->info->getInfosOptions()->getPolNuit()->getCol());
        this->setWin->setNewFont(this->info->getInfosOptions()->getPolNuit()->getFam());
    }
    else if
       (this->modeJ()// ( (h>=sunRise && h<sunSet ) && (modeNuit || init) )
   &&  (!this->info->getInfosOptions()->getModeJour() || init))
    { // Si on est dans le mode nuit ou init et on passe l'heure de la sortie du soleil ou on est avant sunSet, on mis a jour l'image de fond et le mode a Jour
        QString img=QResource(":/Background/Images/jour.png").fileName();
        ui->bckImg->setPixmap(QPixmap(img).scaled(this->size()));
        this->setWin->getBckgImg()->setPixmap(QPixmap(img).scaled(this->size()));
        this->info->getInfosOptions()->setModeJour(true);
    }
}
void MainWindow::actuInfos()
{   // Chaque minute on demande des informations a la balise et la web
    // Utilisation du variable static pour tracker l'evolution des minutes de façon simple
    static double minut=heure->currentDateTime().toString("mm").toDouble();
    if(minut++>=60)
    { // On fait le check du changement de mode que quand on change l'heure
        this->actuBckg();
        minut=1; // On reinit le compteur
    }
    try {
            contactBalise(this->info->getInfosBalise());
            contactWeb(this->info->getInfosVille());
    } catch (InComException &excp) {
        QMessageBox msgBox;
        msgBox.setText(QString().fromStdString(excp.what()));
        msgBox.exec();
    }
    this->actuWindow();
}

void MainWindow::on_actionSettings_triggered()
{
    this->setWin->setWindowState(Qt::WindowNoState); // Pour l'ouvrir normalement (sans minimize)
    setWin->exec();
}

void MainWindow::actuFont()
{
    if(this->info->getInfosOptions()->getModeJour())
    {
        QFont f1(this->info->getInfosOptions()->getPolJour()->getFam(),fSize);
        this->setFont(f1);
        setWin->setFont(f1);
    }
    else
    {
        QFont f1(this->info->getInfosOptions()->getPolNuit()->getFam(),fSize);
        this->setFont(f1);
        setWin->setFont(f1);
    }
}

void MainWindow::translate()
{
    ui->retranslateUi(this);
    this->actuWindow();
}

void MainWindow::actuVille()
{
    this->info->getInfosVille()->setVille(this->info->getInfosOptions()->getVilleChoix());
    try{
        contactWeb(this->info->getInfosVille());
    }
    catch (InComException &excp) {
        QMessageBox msgBox;
        msgBox.setText(QString().fromStdString(excp.what()));
        msgBox.exec();
    }
    this->actuVilleText();
    this->actuGraph();
}
