#include "mainwindow.h"
#include "settingswindow.h"
#include "ui_settingswindow.h"
#include <QTranslator>

SettingsWindow::SettingsWindow(QWidget *parent, InfOptions* infSet) :
    QDialog(parent),
    ui(new Ui::SettingsWindow),
    infSettings(infSet),
    mainW(parent)
{
    ui->setupUi(this);
    this->setWindowTitle("Adminitration");
    this->init();
    this->bckgImg=ui->bckImg;
}

void SettingsWindow::init()
{
    /***Initialisation de l'ecran avec les infos gardes dans InfSettings **/
    /*** Aussi les informations des combo box Ville et Colour sont fournis***/
    ui->colBox->addItems(QColor::colorNames()); // Les couleurs disponibles dans Qt sont fournis dans la combo box de couleurs
    ui->cmbVille->addItems(listVille()); // La list de villes obtenus du fichier telecharge du site web
    QString temp=this->infSettings->getVilleChoix();
    this->infSettings->setVilleChoix(this->gestBox(this->infSettings->getVilleChoix(),ui->cmbVille,vilDefaut,ErrCode::ERR_NF));
    if(temp!=this->infSettings->getVilleChoix()) {(dynamic_cast<MainWindow*> (this->parent()))->actuVille();}
    if(this->infSettings->getFrmtHr12())
    {
        ui->rdb12h->setChecked(true);
    }
    else {
        ui->rdb24h->setChecked(false);
    }
    if(this->infSettings->getFrmtTmpCel())
    {
        ui->rdbCel->setChecked(true);
    }
    else {
        ui->rdbFar->setChecked(true);
    }
    if(this->infSettings->getLangueFr())
    {
        ui->rdbFran->setChecked(true);
    }
    else {
        ui->rdbAng->setChecked(true);
        this->chgLang();
    }
    if (this->infSettings->getModeJour())
    { // On pas par gestBox pour s'assurer que le valeur actuel dans infSettings exist
        ui->rdbJour->setChecked(true);
        //1er Verification des valeurs dans PolNuit
        this->infSettings->getPolNuit()->setCol(this->gestBox(this->infSettings->getPolNuit()->getCol(),ui->colBox,colDefaut,ErrCode::ERR_COL));
        this->infSettings->getPolNuit()->setFam(this->gestBox(this->infSettings->getPolNuit()->getFam(),ui->fontBox,famDefaut,ErrCode::ERR_POL));
        //2eme Verification et mis a jour avec les valeurs PolJour
        this->on_colBox_activated(this->infSettings->getPolJour()->getCol());
        this->infSettings->getPolJour()->setFam(this->gestBox(this->infSettings->getPolJour()->getFam(),ui->fontBox,famDefaut,ErrCode::ERR_POL)); // Si on fait setFont a la construction, la police ne pourrait pas changer apres
    }
    else {
        ui->rdbNuit->setChecked(true);
        //1er Verification des valeurs dans PolJour
        this->infSettings->getPolJour()->setCol(this->gestBox(this->infSettings->getPolJour()->getCol(),ui->colBox,colDefaut,ErrCode::ERR_COL));
        this->infSettings->getPolJour()->setFam(this->gestBox(this->infSettings->getPolJour()->getFam(),ui->fontBox,famDefaut,ErrCode::ERR_POL));
        //2eme Verification et mis a jour avec les valeurs PolNuit
        this->on_colBox_activated(this->infSettings->getPolNuit()->getCol());
        this->infSettings->getPolNuit()->setFam(this->gestBox(this->infSettings->getPolNuit()->getFam(),ui->fontBox,famDefaut,ErrCode::ERR_POL));
    }
    // Conexion des signaux toogled avec les slot de changement de status, la signal toggled s'active quand le button es change d'status
    this->connect(ui->rdb12h,SIGNAL(toggled(bool)),this,SLOT(chgHeure()));
    this->connect(ui->rdbFran,SIGNAL(toggled(bool)),this,SLOT(chgLang()));
    this->connect(ui->rdbCel,SIGNAL(toggled(bool)),this,SLOT(chgT()));
    this->connect(ui->rdbJour,SIGNAL(toggled(bool)),this,SLOT(chgMode()));
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}

void SettingsWindow::chgHeure()
{
    /***La information dans infSettings est mis a jour avec l'etat actuel des buttons heure***/
    if (ui->rdb12h->isChecked())
    {
        infSettings->setFrmtHr12(true);
    }
    else {
        infSettings->setFrmtHr12(false);
    }
}

void SettingsWindow::chgT()
{
    /***La information dans infSettings est mis a jour avec l'etat actuel des buttons Temperature***/
    if (ui->rdbCel->isChecked())
    {
        infSettings->setFrmtTmpCel(true);
    }
    else {
        infSettings->setFrmtTmpCel(false);
    }
    //On mis a jour l'interface graphique
    (dynamic_cast<MainWindow*> (mainW))->actuTemp();
}

void SettingsWindow::chgLang()
{
    /***La information dans infSettings est mis a jour avec l'etat actuel des buttons Langue***/
    if (ui->rdbFran->isChecked())
    {
        infSettings->setLangueFr(true);
        if (this->infSettings->instaled)
        {
            qApp->removeTranslator(&this->infSettings->t);
            qApp->removeTranslator(&this->infSettings->guiTranslator);
            ui->retranslateUi(this);
            (dynamic_cast<MainWindow*> (this->mainW))->translate();
            this->infSettings->instaled = false;
        }
    }
    else {
        infSettings->setLangueFr(false);
         qApp->installTranslator(&this->infSettings->t);
         qApp->installTranslator(&this->infSettings->guiTranslator);
         ui->retranslateUi(this);
         (dynamic_cast<MainWindow*> (this->parent()))->translate();
         this->infSettings->instaled = true;
    }
}

void SettingsWindow::chgMode()
{
    /***Quand le mode change, la information dans infSettings est mis a jour avec l'etat actuel des buttons Mode***/
    /*** Ausi on mis a jour les combo box de Col et Fam avec le mode active, on utilise la fonction gestBox pour s'assurer que l'info garde dans infSettings existe dans la box**/
    if (ui->rdbJour->isChecked())
    {
        this->infSettings->getPolJour()->setCol(this->gestBox(this->infSettings->getPolJour()->getCol(),ui->colBox,colDefaut,ErrCode::ERR_COL));
        this->infSettings->getPolJour()->setFam(this->gestBox(this->infSettings->getPolJour()->getFam(),ui->fontBox,famDefaut,ErrCode::ERR_POL));
    }
    else {
        this->infSettings->getPolNuit()->setCol(this->gestBox(this->infSettings->getPolNuit()->getCol(),ui->colBox,colDefaut,ErrCode::ERR_COL));
        this->infSettings->getPolNuit()->setFam(this->gestBox(this->infSettings->getPolNuit()->getFam(),ui->fontBox,famDefaut,ErrCode::ERR_POL));
    }
}

QString SettingsWindow::gestBox(QString val, QComboBox *combo,QString valDef,ErrCode err)
{
    /***Fonction qui sert a gerer les combo box**/
    /* - > Il verifie si le val existe dans la combo box*/
    /* - > S'il existe, il assigne le valeur, au contraire, il assigne un valeur par defaut et il lance une fenetre avec un message d'erreur */
    int index; // Variable temp qui garde l'index des comboBox (verification du existance)
    index = combo->findText(val);
    if (index!=-1)
    {
        combo->setCurrentIndex(index);
    }
    else {
        QMessageBox msgBox;
        msgBox.setText(InComException(err,val.toStdString()).what());
        msgBox.exec();
        combo->setCurrentIndex(combo->findText(valDef));
    }
    return combo->currentText();
}

QStringList SettingsWindow::listVille()
{ /***Lecture du fichier avec le liste de villes disponibles dans l'API Web et retour les villes dans une QStringList***/
     QString resName=QDir::currentPath()+"/../SystemeEmbarque/current_name.txt"; // Obtention du fichier current_names dans le dossier des fichiers du Soft
     bool openOk;
     QFile file(resName);
     QString contenuFichier;
     QStringList listVil;
     openOk = file.open(QIODevice::ReadOnly|QIODevice::Text);
     if (openOk)
     {
         contenuFichier = file.readAll();
         for(QString line: contenuFichier.split("\n"))
         { // Si le nom de la ville est entre ", on le filtre avec un split
             if (line.indexOf("\"")!=-1)
             {
                 listVil.append(line.split("\"")[1]);
             }//Si non et la ligne n'est pas vide, on append la ville
             else if(line.isEmpty()) {
                 listVil.append(line);
             }
         }
     }
     else {
         file.close(); // On ferme le fichier avant de faire la gestion d'erreur
    }
     file.close();
     return listVil;
}

void SettingsWindow::setPal(QString val)
{
    QPalette pal = this->palette();
    pal.setColor(QPalette::WindowText,QColor(val));
    this->setPalette(pal);
    mainW->setPalette(pal);
    //A futur, utiliser une option plus optim
    ui->boxLang->setPalette(pal);
    ui->boxMode->setPalette(pal);
    ui->boxTemp->setPalette(pal);
    ui->boxHeure->setPalette(pal);
    ui->boxStyle->setPalette(pal);
    ui->cmbVille->setPalette(pal);
    ui->colBox->setPalette(pal);
    ui->fontBox->setPalette(pal);
}

void SettingsWindow::on_colBox_activated(const QString &arg1)
{ // Quand le box est active avec un nouvelle text, la info est mis a jour dans l'structure
    if(ui->rdbJour->isChecked())
    {
        this->infSettings->getPolJour()->setCol(this->gestBox(arg1,ui->colBox,colDefaut,ErrCode::ERR_COL));
    }
    else {
        this->infSettings->getPolNuit()->setCol(this->gestBox(arg1,ui->colBox,colDefaut,ErrCode::ERR_COL));
    }
    if( (this->infSettings->getModeJour()&&ui->rdbJour->isChecked())
    ||  (!this->infSettings->getModeJour()&&!ui->rdbJour->isChecked() ))
    { // On mis a jour la palette si on modifie le mode actuel
        this->setPal(arg1);
    }
}

void SettingsWindow::setNewFont(QString val)
{
    this->setFont(QFont(val,fSize));
    mainW->setFont(QFont(val,fSize));
}

void SettingsWindow::on_fontBox_activated(const QString &arg1)
{// Quand le box est active avec un nouvelle text, la info est mis a jour dans l'structure
    if(ui->rdbJour->isChecked())
    {
        this->infSettings->getPolJour()->setFam(this->gestBox(arg1,ui->fontBox,famDefaut,ErrCode::ERR_POL));
    }
    else {
        this->infSettings->getPolNuit()->setFam(this->gestBox(arg1,ui->fontBox,famDefaut,ErrCode::ERR_POL));
    }
    if( (this->infSettings->getModeJour()&&ui->rdbJour->isChecked())
    ||  (!this->infSettings->getModeJour()&&!ui->rdbJour->isChecked() ))
    { // On mis a jour la font si on modifie le mode actuel
        this->setNewFont(arg1);
    }
}

void SettingsWindow::on_cmbVille_activated(const QString &arg1)
{// Quand le box est active avec un nouvelle text, la info est mis a jour dans l'structure
    this->infSettings->setVilleChoix(this->gestBox(arg1,ui->cmbVille,vilDefaut,ErrCode::ERR_NF));
    (dynamic_cast<MainWindow*> (this->parent()))->actuVille();
}
