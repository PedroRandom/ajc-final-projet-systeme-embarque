#include "infclass.h"

Pol::Pol(QString f, QString c)
  : fam(f),col(c)
{

}

Pol::~Pol()
{
    if(comments)
    {
        ostringstream oss;
        oss<<"Destruction de \n "<<this->infos();
        cout<<oss.str()<<endl;
    }
}

void Pol::setFam(const QString &value)
{
    fam = value;
}

void Pol::setCol(const QString &value)
{
    col = value;
}

string Pol::infos()
{
    ostringstream oss;
    oss<<"Infos du Police"<<endl;
    oss<<" - Famille "<<this->getFam().toStdString()<<endl;
    oss<<" - Coleur "<<this->getCol().toStdString()<<endl;
    return oss.str();
}

InfBal::InfBal(double t, double h, double p)
    :temp(t),humid(h),pres(p)
{
    this->setErr(InComException());// Assignation de No Error
}

InfBal::~InfBal()
{
    if (comments)
    {
        ostringstream oss;
        oss<<"Destruction de: "<<endl<<this->infos();
        cout<<oss.str();
    }
}

void InfBal::setTemp(double value)
{
    if (value<-40 || value>50)
    {
        temp=-99;
        InComException excp(ErrCode::ERR_TMP,"");
        this->err = excp; // On ne fait throw parce qu'on doit lire encore les autres infos
    }
    else {
        temp = value;
    }
}


void InfBal::setHumid(double value)
{
    humid = value;
}

void InfBal::setPres(double value)
{
    pres = value;
}

void InfBal::setErr(const InComException &value)
{
    err = value;
}

string InfBal::infos()
{
    ostringstream oss;
    oss<<"Information de la balise :"<<endl;
    oss<<" - Temp : "<<this->getTemp()<<endl;
    oss<<" - Humid : "<<this->getHumid()<<endl;
    oss<<" - Pression : "<<this->getPres()<<endl;
    oss<<" - Erreur : "<<this->err.what()<<endl;
    return oss.str();
}

InfVill::InfVill(QString d,double t, double h, double p,QString ic, QString v, InComException e)
    : InfJournee(d,t,h,p,ic,e),ville(v)
{

}

InfVill::~InfVill()
{
    if (comments)
    {
        ostringstream oss;
        oss<<"Destruction de"<<endl<<this->infos();
        cout<<oss.str()<<endl;
    }
}

InfJournee::InfJournee(QString d, double t, double h,double p ,QString ic, InComException e)
    :   date(d),humid(h),pres(p),icon(ic),err(e)
{
    this->setTemp(t); // La temperature doit être fait apres la Zone de Initialization, parce que s'il y a une temperature incorrect l'erreur doit etre modifie
}

InfJournee::~InfJournee()
{
    if (comments)
    {
        ostringstream oss;
        oss<<"Destruction de "<<endl<<this->infos();
        cout<<oss.str(); //Utilisation du oss pour eviter la separation du message
    }
}

void InfJournee::setTemp(double value)
{
    if (value<-40 || value>=50)
    {
        temp=-99;
        InComException excp(ErrCode::ERR_TMP,"");
        this->err = excp; // On ne fait throw parce qu'on doit lire encore les autres infos
    }
    else {
        temp = value;
    }
}

void InfJournee::setHumid(double value)
{
    humid = value;
}
void InfJournee::setIcon(const QString &value)
{
    icon = value;
}

void InfVill::setVille(const QString &value)
{
    ville = value;
}

void InfVill::setLstPrev(const vector<InfJournee> &value)
{
    lstPrev = value;
}

string InfVill::infos()
{
    ostringstream oss;
    oss<<"Info de la ville "<<this->getVille().toStdString()<<endl;
    oss<<" - Ville : "<<this->getVille().toStdString()<<endl;
    oss<<InfJournee::infos();
    oss<<"Previsions a future : "<<endl;
    for(auto val: this->getLstPrev())
    {
        oss<<val.infos()<<endl;
    }
    oss<<"------Fin d'infos------"<<endl;
    return oss.str();
}

void InfJournee::setErr(const InComException &value)
{
    if(value.getCode()==ErrCode::ERR_COM){
        this->setIcon(imgUnknown);
    };
    err = value;
}

string InfJournee::infos()
{
    ostringstream oss;
    oss<<"Previsions de : "<<this->getDate().toStdString()<<endl;
    oss<<" - Temp : "<<this->getTemp()<<endl;
    oss<<" - Humid : "<<this->getHumid()<<endl;
    oss<<" - Pression : "<<this->getPres()<<endl;
    oss<<" - Icon : "<<this->getIcon().toStdString()<<endl;
    oss<<" - Erreur : "<<this->getErr().what()<<endl;
    return oss.str();
}

QString InfJournee::getIconFile()
{
    QString resName=QString(":/Icons/Icons/%1%2").arg(this->getIcon()).arg(imgForm); // Name du resource du classification "Image" dans le dossier "Image"
    if (QResource(resName).isValid())
    {
        return QResource(resName).fileName();
    }
    else { // Si le resource n'existe pas, on utilise l'image unknown
        return QResource(QString(":/Icons/Icons/%1%2").arg(imgUnknown).arg(imgForm)).fileName();
    }
}

void InfJournee::setDate(const QString &value)
{
    date = value;
}

void InfJournee::setPres(double value)
{
    pres = value;
}

double InfClass::tempFromCtoF(double tempC,bool convert)
{ //Fais la conversion du Celsius a Farenheit si convert est active
    if (convert)
    {
        return ((tempC*9/5)+32);
    }
    else {
        return tempC;
    }
}

QString InfClass::genTstring(double tempC, bool fmrtTinC)
{
    if (fmrtTinC)
    {
        return QString::number(tempC,'f',2)+" Celsius";
    }
    else
    {
        return QString::number(InfClass::tempFromCtoF(tempC,true),'f',2)+" Farenheit";
    }
}

InfClass::InfClass(bool fHr, bool fTmp, bool lang, bool mJour,QString vilChoix,QString pJour,QString cJour,QString pNuit, QString cNuit, double tBal, double hBal, double presBal,QString date, double tVil, double hVil, double pVil ,QString ic, QString vil,InComException erVil)
{
    this->infosOptions = new InfOptions(fHr,fTmp,lang,mJour,vilChoix,pJour,cJour,pNuit,cNuit);
    this->infosBalise = new InfBal(tBal,hBal,presBal);
    this->infosVille = new InfVill(date,tVil,hVil,pVil,ic,vil,erVil);
}

InfClass::~InfClass()
{
    if (comments)
    {
        cout<<"Destruction de InfClass, des Infos Ville et Infos Balise "<<endl;
    }
        delete this->getInfosVille();
        delete this->getInfosBalise();
        delete this->getInfosOptions();
}

vector<QString> InfOptions::paramNames{
"/infos/frmtHr12","/infos/frmtTmpCel","/infos/langueFr","/infos/villeChoix",
"/infos/polJour/Fam","/infos/polJour/Col","/infos/polNuit/Fam",
"/infos/polNuit/Col"
};

InfOptions::InfOptions(bool fHr, bool fT, bool lang, bool mJ,QString v, QString fJour,QString cJour,QString fNuit,QString cNuit,QString t, QString guiTranslator,bool instaled)
    : frmtHr12(fHr),frmtTmpCel(fT),langueFr(lang),modeJour(mJ),villeChoix(v)
{
    this->polJour=new Pol(fJour,cJour);
    this->polNuit=new Pol(fNuit,cNuit);
    this->loadParams(); // Charge les parametres gardes dans le fichier init
    this->t.load(t);
    this->guiTranslator.load(guiTranslator);
    this->instaled=instaled;
}

InfOptions::~InfOptions()
{
    this->saveParams(); // A eliminer de façon que on le farait seulement quand on sort du menu Administration! Parce que si non, il ne fait pas du sense
    if (comments)
    {
        ostringstream oss;
        oss<<"Destruction de "<<endl<<this->infos();
        cout<<oss.str();
    }
    delete this->getPolJour();
    delete this->getPolNuit();
}

void InfOptions::setFrmtHr12(bool value)
{
    frmtHr12 = value;
}


void InfOptions::setFrmtTmpCel(bool value)
{
    frmtTmpCel = value;
}

void InfOptions::setLangueFr(bool value)
{
    langueFr = value;
}

void InfOptions::setModeJour(bool value)
{
    modeJour = value;
}

void InfOptions::setPolJour(Pol *value)
{
    polJour = value;
}

void InfOptions::setPolNuit(Pol *value)
{
    polNuit = value;
}

void InfOptions::setVilleChoix(const QString &value)
{
    villeChoix = value;
}

void InfOptions::loadParams()
{
    QSettings params(paramFile,QSettings::IniFormat);
    for (QString key:InfOptions::paramNames) {
        QVariant temp = params.value(key);
        if (!temp.isNull()) //On verifie que le parametre est dans le fichier de parametres
        {
            if (key==InfOptions::paramNames[0])
            {
                this->setFrmtHr12(temp.toBool());
            }
            else if (key==InfOptions::paramNames[1]) {
                this->setFrmtTmpCel(temp.toBool());
            }
            else if (key==InfOptions::paramNames[2]) {
                this->setLangueFr(temp.toBool());
            }
            else if (key==InfOptions::paramNames[3]) {
                this->setVilleChoix(temp.toString());
            }
            else if (key==InfOptions::paramNames[4]) {
                this->getPolJour()->setFam(temp.toString());
            }
            else if (key==InfOptions::paramNames[5]) {
                this->getPolJour()->setCol(temp.toString());
            }
            else if (key==InfOptions::paramNames[6]) {
                this->getPolNuit()->setFam(temp.toString());
            }
            else if (key==InfOptions::paramNames[7]) {
                this->getPolNuit()->setCol(temp.toString());
            }
        }
    }
}


void InfOptions::saveParams()
{
    QSettings params(paramFile,QSettings::IniFormat);
    params.setValue(InfOptions::paramNames[0],this->getFrmtHr12());
    params.setValue(InfOptions::paramNames[1],this->getFrmtTmpCel());
    params.setValue(InfOptions::paramNames[2],this->getLangueFr());
    params.setValue(InfOptions::paramNames[3],this->getVilleChoix());
    params.setValue(InfOptions::paramNames[4],this->getPolJour()->getFam());
    params.setValue(InfOptions::paramNames[5],this->getPolJour()->getCol());
    params.setValue(InfOptions::paramNames[6],this->getPolNuit()->getFam());
    params.setValue(InfOptions::paramNames[7],this->getPolNuit()->getCol());
}

string InfOptions::infos()
{
    ostringstream oss;
    oss<<"Information de Info Options : "<<endl;
    oss<<" - Format 12 H"<<this->getFrmtHr12()<<endl;
    oss<<" - Format Temp C"<<this->getFrmtTmpCel()<<endl;
    oss<<" - Langue Fr "<<this->getLangueFr()<<endl;
    oss<<" - Mode Jour "<<this->getModeJour()<<endl;
    oss<<" - Ville "<<this->getVilleChoix().toStdString()<<endl;
    return oss.str();
}

void InfClass::setInfosBalise(InfBal *value)
{
    infosBalise = value;
}

void InfClass::setInfosVille(InfVill *value)
{
    infosVille = value;
}

void InfClass::setInfosOptions(InfOptions *value)
{
    infosOptions = value;
}

string InfClass::infos()
{
    ostringstream oss;
    oss<<this->getInfosVille()->infos();
    oss<<this->getInfosBalise()->infos();
    oss<<this->getInfosOptions()->infos();
    return oss.str();
}


