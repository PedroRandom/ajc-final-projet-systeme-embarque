#ifndef MODBALISE_H
#define MODBALISE_H
#include "incomexception.h"
#include "infclass.h"
#include <QWidget>
#include <QProcess>
#include <QDebug>
#include <QFile>
#include <QDir>
void getErreurs(InfBal *,QProcess*);
void launchProcess(QString,QStringList);
void lectureFichier(InfBal *);
void contactBalise(InfBal*);
#endif // MODBALISE_H
