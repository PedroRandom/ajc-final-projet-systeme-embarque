#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>
#include <QMessageBox>
#include <QComboBox>
#include <QFile>
#include <QDir>
#include <QDebug>

#include <infclass.h>


namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsWindow(QWidget *parent = nullptr, InfOptions* infSettings = nullptr);
    void init();
    ~SettingsWindow();
    QLabel* getBckgImg(){return this->bckgImg;}
    void setPal (QString val);
    void setNewFont(QString val);
private slots:
    void chgHeure();
    void chgT();
    void chgLang();
    void chgMode();
    void on_fontBox_activated(const QString &arg1);
    void on_colBox_activated(const QString &arg1);
    void on_cmbVille_activated(const QString &arg1);

private:
    Ui::SettingsWindow *ui;
    InfOptions* infSettings;
    QString gestBox(QString,QComboBox*,QString,ErrCode);
    QStringList listVille();
    QWidget *mainW;
    QLabel *bckgImg;
};

#endif // SETTINGSWINDOW_H
