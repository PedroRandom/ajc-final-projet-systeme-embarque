#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>
#include <QTimer>
#include <QMessageBox>

#include "incomexception.h"
#include "infclass.h"
#include "modbalise.h"
#include "modville.h"
#include "settingswindow.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setUpTimers(); // Initialise les timers qui mettent a jour l'horlogue et les infos
    void initText(); // Initialise les text fixes de l'ecran main
    void setTextLabels(vector<QLabel*>,vector<QString>); // Set les texts du labels Introduits avec les vecteurs introduits
    void actuBalText(); // Mis a jour le text avec les valeurs reçus de la balise
    void actuVilleText(); // Mis a jour le text avec les valeur reçus du ville
    void actuGraph(); // Mis a jour les valeurs du graphique avec les valeurs des previsions recues
    void actuTemp(); // Mis a jour les valeurs du Temperature (Ville, Balise et Graph), utilisé quand le format est changé
    void actuWindow(); //Mise à jour de l'écran MainWindows avec tous les infos dynamiques
    void actuBckg(bool=false); // Mis à jour du Backgound
    void translate(); // Lance ui->retranslateUi() et actuWindow (La translation elimgine les valeurs des labels et les dates doivent changer le format)
    bool modeJ(); // Return True si (heure>=sunRise && heure<sunSet )
    void actuVille(); // Quand le ville est change dans la fenetre, les infos du web doivent etre mis a jour
private slots:
    void actuTime(); // Mis a jour l'heure de l'horlogue
    void actuInfos(); // Mis a jour l'info du balise et de la ville
    void on_actionSettings_triggered();
    void actuFont(); // La mis a jour de font mis de problemes si c'est fait aux moment du constructeur --> Utilisation d'un event unique avec un Timer pour l'initialiser

private:
    Ui::MainWindow *ui;
    QDateTime *heure; //Objet Heure
    QTimer *timerHeure; // Declencher la mis a jour de l'heure
    QTimer *timerInfos; //Declencher la mis a jour des infos
    InfClass* info; // Objet avec tous les infos necesaires
    SettingsWindow* setWin; // Ecran des settings

};

#endif // MAINWINDOW_H
